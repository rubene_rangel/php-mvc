<?php
defined('BASEPATH') or exit('No direct access');

spl_autoload_register(function ($class) {
  if (is_file(CORE . "$class.php"))
    //require CORE . "$class.php";
    return require CORE . "$class.php";
  if (is_file(HELPER_PATH . "$class.php"))
    return require HELPER_PATH . "$class.php";
  /* else {
    echo "No se encontro la clase $class";
  } */
});
