<?php
defined('BASEPATH') or exit('No direct access');

/**
 * undocumented class
 */
class Model
{
  protected $db;

  public function __construct()
  {
    // $this->db = new Mysqli("localhost", "root", "", "db773440853");
    $this->db = new Mysqli(HOST, USER, PASSWORD, DB_NAME);
  }

  public function __destruct()
  {
    $this->db->close();
  }
}
