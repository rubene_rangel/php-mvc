<?php
defined("BASEPATH") or exit("No direct access");

define("TITLE", 'PHP MVC INITIAL');

/* Valores URI */
define("URI", $_SERVER["REQUEST_URI"]);
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

define('LIBS_ROUTE', ROOT . '/system/libs/');

/* Valores de Core*/
define("CORE", "system/core/");
define("DEFAULT_CONTROLLER", "Home");

/* Valores de Rutas */
define("PATH_CONTROLLERS", "app/controllers/");
define("PATH_VIEWS", "app/views/");

define("HELPER_PATH", "system/helpers/");
define("REQUEST_METHOD", $_SERVER["REQUEST_METHOD"]);

// define("FOLDER_PATH", "/");
define("FOLDER_PATH", "");

/* Valores de Base de Datos */
define("HOST", "");
define("USER", "");
define("PASSWORD", "");
define("DB_NAME", "");

define("ERROR_REPORTING_LEVEL", -1);
