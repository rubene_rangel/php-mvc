<?php
define("BASEPATH", true);
//echo $_SERVER['REQUEST_URI'];
require "system/config.php";
require "system/core/autoload.php";
/* require "system/core/Router.php";
require "system/core/Controller.php"; */
error_reporting(ERROR_REPORTING_LEVEL);

// var_dump($_SERVER["REQUEST_URI"]);

$router = new Router();

/* echo "<pre>";
print_r($router->getUri());
echo "</pre>"; */

$controller = $router->getController();
$method = $router->getMethod();
$param = $router->getParam();

/**
 * Validaciones e inclusión del controlador y el metodo 
 */
if (!CoreHelper::validateController($controller))
  // throw new Exception("Controlador $controller no fue encontrado");
  $controller = "ErrorPage";

// var_dump($ruat);
require PATH_CONTROLLERS . strtolower($controller) . "/{$controller}Controller.php";
$controller .= "Controller";

if (!CoreHelper::validateMethodController($controller, $method))
  // throw new Exception("El método {$method} del controlador {$controller} no fue encontrado");
  $method = 'exec';


/* echo "Controlador: {$controller} <br>";
echo "Método: {$method} <br>";
echo "Param: {$param} <br>"; */

/**
 * Ejecución final del controlador, método y parámetro obtenido por URI
 */
$controller = new $controller();
$controller->$method($param);
