<?php defined("BASEPATH") or exit("No direct access"); ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>DashBoard</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
  <nav class="nav navbar-nav navbar-right">
    <div class="container">
      <div class="navbar-header">
        <a href="/main" class="navbar-brand">Indigos Network</a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?= "/main/form" ?>">Add Client</a></li>
        <li><a href="/main/clientsList">Client List</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $email ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/main/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container text-center">
    <form action="/main/search" class="form-inline" method="POST">
      <div class="form-group text-center">
        <label for="search">Buscar por nombre</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Name">
      </div>
      <button type="submit" class="btn btn-default">Search</button>
    </form>
  </div>

  <div class="container">
    <?php !empty($show_form) ? require ROOT . "app/views/Main/form.php" : "" ?>
    <?php !empty($show_edit_form) ? require ROOT . "app/views/Main/edit_form.php" : "" ?>
    <?php !empty($show_client_list) ? require ROOT . "app/views/Main/client_list.php" : "" ?>
  </div>

  <!--jQuery-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>