
<div class="row">
  <div class="col-md-12">
    <h2>Clients List</h2>
    <div class="row text-center">
      <?php !empty($message) ? print("<div class='alert alert-$message_type'>$message</div>") : ""; ?>
    </div>
    <table class="table table-striped">
      <thead>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Edit</th>
        <th>Remove</th>
      </thead>
      <tbody>
        <?php
          $tr = "";
          foreach ($clients as $row)
          {
            $tr .= "<tr>";
            $tr .= "<td>{$row['id']}</td>";
            $tr .= "<td>{$row['name']}</td>";
            $tr .= "<td>{$row['email']}</td>";
            $tr .= "<td>{$row['address']}</td>";
            $tr .= "<td><a href='/Main/clientList/{$row['id']}'>Edit</a></td>";
            $tr .= "<td><a href='/Main/removeClient/{$row['id']}'>Remove</a></td>";
            $tr .= "</tr>";
            
          }
          echo $tr;
        ?>  
      </tbody>
    </table>
  </div>
</div>



