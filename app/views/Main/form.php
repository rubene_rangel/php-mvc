

<div class="row">
  <div class="col-md-6">
    <h2>Add Client Form</h2>
    <form action="/main/addClient" method="post">
      
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Name">
      </div>

      <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" name="email" id="email" placeholder="Email" class="form-control">
      </div>

      <div class="form-group">
        <label for="address">Address</label>
        <input type="text" name="address" id="address" class="form-control" id="address" placeholder="Address">
      </div>
      <div class="row">
        <?php
          !empty($message) ? print("<div class='alert alert-warning'>$message</div>") : "";
        ?>
      </div>
      
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>


