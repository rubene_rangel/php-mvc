<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Home/HomeModel.php";

class HomeController extends Controller
{
  /* 
  * string
  */
  public $nombre;

  /*
  *object
  */
  private $model;

  public function __construct()
  {
    // var_dump(__CLASS__);
    $this->model = new HomeModel();
    $this->nombre = "MVC";
  }

  // public function exec($param)
  public function exec()
  {
    //  echo "<h1>Hola Word</h1>";
    //$this->render();

    //var_dump($param);
    $this->show();
  }

  // public function show($param)
  public function show()
  {
    // $params = array("nombre" => "Ruben", "canal" => "IndigosNetwork");
    // $params = array("nombre" => $param["email"], "parametro" => $param);
    // $params = array("nombre" => $param["email"]);
    $params = array("nombre" => $this->nombre);
    $this->render(__CLASS__, $params);
  }

  public function getUser($id)
  {
    $user = $this->model->getUser($id);
    $this->nombre = $user["email"];
    $this->show();
  }
}
