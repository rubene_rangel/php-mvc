<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Login/LoginModel.php";
require_once LIBS_ROUTE . 'Session.php';
/**
 * undocumented class
 */
class LoginController  extends Controller
{
  private $model;

  private $session;

  public function __construct()
  {
    $this->model = new LoginModel();
    $this->session = new Session();
  }

  public function signin($request_params)
  {
    // var_dump($request_params);
    if ($this->verify($request_params))
      return $this->renderErrorMessage("El email y password son obligatorios");

    $result = $this->model->signIn($request_params["email"]);
    // var_dump($result);
    if (!$result->num_rows)
      return $this->renderErrorMessage("El email {$request_params["email"]} no fue encontrao");

    $result = $result->fetch_object();

    /* var_dump($result);
    exit(); */

    if (!password_verify($request_params["password"], $result->password))
      return $this->renderErrorMessage("El password es incorrecto");

    /* Iniciar Sesion */
    $this->session->init();
    $this->session->add("email", $result->email);
    var_dump($this->session->getAll());
    header("Location:  /main");
  }

  public function verify($request_params)
  {
    return empty($request_params["email"]) || empty($request_params["password"]);
  }

  public function renderErrorMessage($message)
  {
    $params = array("error_message" => $message);
    $this->render(__CLASS__, $params);
  }

  public function exec()
  {
    $this->render(__CLASS__);
  }
}
