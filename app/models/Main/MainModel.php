<?php
defined('BASEPATH') or exit('No direct access');
/**
 * Model to table CLIENTS
 */
class MainModel extends Model
{
  public function __contruct()
  {
    parent::__construct();
  }

  public function affected_rows()
  {
    return $this->db->affected_rows;
  }

  public function add($params)
  {
    $name   = $this->db->real_escape_string($params["name"]);
    $email    = $this->db->real_escape_string($params["email"]);
    $address  = $this->db->real_escape_string($params["address"]);
    $sql      = "INSERT INTO `clients` (name, email, address) VALUES ('$name', '$email', '$address')";
    return $this->db->query($sql);
  }

  public function clientsList()
  {
    $sql = "SELECT * FROM `clients` ";
    return $this->db->query($sql);
  }

  public function clientList($id)
  {
    $sql = "SELECT * FROM `clients` WHERE id = {$id};";
    return $this->db->query($sql);
  }

  public function update($params)
  {
    //var_dump($params);
    $name     = $this->db->real_escape_string($params["name"]);
    $email    = $this->db->real_escape_string($params["email"]);
    $address  = $this->db->real_escape_string($params["address"]);
    $id       = $this->db->real_escape_string($params["id"]);

    $sql      = "UPDATE `clients` SET 
                  name = '{$name}', 
                  email = '{$email}',
                  address = '{$address}'
                WHERE
                  id = $id";
    //var_dump($sql);
    return $this->db->query($sql);
  }

  public function remove($id)
  {
    $sql = "DELETE FROM `clients` WHERE id = {$id}";
    return $this->db->query($sql);
  }

  public function search(String $string)
  {
    $sql = "SELECT * FROM `clients` WHERE name LIKE '%{$string}%'";
    //var_dump($sql);
    return $this->db->query($sql);
  }
}
